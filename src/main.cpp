#include <Wire.h>
#include "PCF8591.h" // https://github.com/overbog/PCF8591

#define I2C_SDA 0
#define I2C_SCL 2
#define PCM8591_ADRESS 0x48

PCF8591 pcf8591(PCM8591_ADRESS,0);

////////////////////////////////////////////////////////////////
void setup() {
    Wire.begin(I2C_SDA,I2C_SCL);
    Serial.begin(115200); // initialize serial communication
    pcf8591.begin();
}

////////////////////////////////////////////////////////////////
// Right align a value (type long) for column algined text output.
// For ADC can get large numbers and int too small.
void printLongRight(byte fieldSize, long v) {
char str[10];
   ltoa(v,str,10);
   int len = strlen(str);
   if (len>fieldSize) len = fieldSize;
   int spc = fieldSize-len;
   for( int i=0;i<spc;i++) Serial.print(' ');
   for( int i=0;i<len;i++) {
      Serial.print(str[i]);
   }
}

////////////////////////////////////////////////////////////////
void loop() {

   uint8_t res[4];

   int ana0V = pcf8591.adc_raw_read(0);
   int ana1V = pcf8591.adc_raw_read(1);
   int ana2V = pcf8591.adc_raw_read(2);
   int ana3V = pcf8591.adc_raw_read(3);

   Serial.print("A0 ANA ");  printLongRight(6,ana0V);
   Serial.print(" A1 ANA ");   printLongRight(6,ana1V);
   Serial.print(" A2 ANA  ");   printLongRight(6,ana2V);
   Serial.print(" A3 ANA ");    printLongRight(6,ana3V);
   Serial.println();

   pcf8591.adc_bulk_raw_read(res);

   Serial.print("A3 raw ");Serial.println(res[3]);

   Serial.print("Read VCC (assumed  Vref of 5V): ");
   Serial.println(pcf8591.adc_read(3,5));

   // Write the ADC value from the pot to Aout.
   pcf8591.dac_write(pcf8591.adc_raw_read(3));
   delay(1000);
}
